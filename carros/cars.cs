﻿using System;
using System.Collections.Generic;
using System.Text;

namespace carros
{
    class cars
    {
        public int Ano { get; set; }
        public int NumeroDonos { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Km { get; set; }
        public int Preco { get; set; }
        public string Cor { get; set; }
         public double ValorParcela(int parcelas)
         {
        Console.WriteLine("calculando parcela");
            if (parcelas > 24)

                return 0; 

            double ValorParcela =
            this.Preco / parcelas;

        return ValorParcela;
         }

        public string TipoCarro()
        {
        Console.WriteLine("verificando o tipo do carro");
            int idadeCarro =
                int.Parse(DateTime.Now.Year.ToString()) - this.Ano;

            double kmPorAno = this.Km / idadeCarro;

            if(this.Km == 0) 
            { 
              return "zero";
            }
            
            else if(idadeCarro <= 3)
            {
                if (this.NumeroDonos == 1)
                {
                    if(kmPorAno <= 20000)
                    {
                        return "seminovo";
                    }
                    else
                    {
                        return "usado";
                    }
                }
                else
                {
                    return "usado";
                }
                
            }
            else
            {
                return "usado";
            }

            
        }



    }

   
}
