﻿using System;

namespace Cars
{
    class Program
    {
        static void Main(string[] args)
        {

            Carro carro = new Carro();
            carro.Marca = "Ford";
            carro.Modelo = "Mustang";
            carro.Preco = 49000;
            carro.Km = 20000;
            carro.Ano = 2016;
            carro.NumeroDonos = 1;

            double valorParcela = carro.ValorParcela(20);

            string tipoCarro = carro.TipoCarro();
            Console.WriteLine(valorParcela);
            Console.WriteLine(tipoCarro);
           
        }
    }
}
