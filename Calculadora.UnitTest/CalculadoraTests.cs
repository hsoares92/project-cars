using NUnit.Framework;
using Calculadora;

namespace Calculadora.UnitTest
{
    public class CalculadoraTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForSomar_retornaSoma()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 2;
            calculadora.campo2 = 1;
            calculadora.tipodecalculo = "+";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.EqualTo(3));


            
        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForSubtrair_retornaSub()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 2;
            calculadora.campo2 = 1;
            calculadora.tipodecalculo = "-";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.EqualTo(1));



        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForMultiplicar_retornaMulti()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 2;
            calculadora.campo2 = 1;
            calculadora.tipodecalculo = "*";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.EqualTo(2));



        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForDividir_retornaDivisao()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 2;
            calculadora.campo2 = 1;
            calculadora.tipodecalculo = "/";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.EqualTo(2));



        }
        [Test]
        public void ExecutarCalculo_QuandoAcaoDivisaofor0_retornaDivisao()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 1;
            calculadora.campo2 = 0;
            calculadora.tipodecalculo = "/";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.EqualTo(double.PositiveInfinity));



        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForDividir0por0_retornaNaN()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 0;
            calculadora.campo2 = 0;
            calculadora.tipodecalculo = "/";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.NaN);



        }
    }
}