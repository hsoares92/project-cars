﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cars
{
    class Carro
    {
        public int Ano { get; set; }
        public int NumeroDonos { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Km { get; set; }
        public double Preco { get; set; }
        public string Cor { get; set; }

        public double ValorParcela(int parcelas)
        {
            Console.WriteLine("calculando parcela");
            if (parcelas > 24)
                return 0;

            double valorParcela = 
                this.Preco / parcelas;
            return valorParcela;
        }

        public string TipoCarro()
        {
            Console.WriteLine("verificando tipo de carro");

            int IdadeCarro = int.Parse(DateTime.Now.Year.ToString()) - this.Ano;
            double KmPorAno = this.Km / IdadeCarro;
            //verificar se o carrro é zero
            if(this.Km == 0)
            {
                return "Zero";
            }
            

            else if(IdadeCarro <= 3)
            {
                if(this.NumeroDonos == 1)
                {
                    if(KmPorAno <= 20000)
                    {
                        return "Seminovo";
                    }
                    else
                    {
                        return "usado";
                    }       
                }
                else
                {
                    return "Usado";
                }

            } else
            {
                return "Usado";
            }
        }
    }
}
